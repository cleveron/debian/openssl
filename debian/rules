#!/usr/bin/make -rf
# Copyright 1994, 1995 Ian Jackson.
# I hereby give you perpetual unlimited permission to copy,
# modify and relicense this file, provided that you do not remove
# my name from the file itself.  (I assert my moral right of
# paternity under the Copyright, Designs and Patents Act 1988.)

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

SHELL=/usr/bin/bash
export DEB_BUILD_MAINT_OPTIONS=abi=+lfs,+time64

CONFARGS:= \
	--libdir=lib/$(DEB_HOST_MULTIARCH) \
	--openssldir=/usr/lib/ssl \
	--prefix=/usr \
	disable-afalgeng \
	disable-capieng \
	disable-dtls1 \
	disable-dtls1-method \
	disable-idea \
	disable-mdc2 \
	disable-sm3 \
	disable-tls1 \
	disable-tls1_1 \
	enable-ec_nistp_64_gcc_128 \
	enable-ktls

# Support build parallelism.
ifneq ($(filter parallel=%,$(DEB_BUILD_OPTIONS)),)
	NUMJOBS=$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	MAKEFLAGS+=-j$(NUMJOBS)
endif

ifneq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
	CONFARGS+=disable-tests
endif

# For generating the manpages
export VERSION=$(DEB_VERSION_UPSTREAM)

# Base system package, downgrade architecture level from default.
ifeq ($(DEB_HOST_ARCH),amd64)
	CFLAGS+=-march=ivybridge
else ifeq ($(DEB_HOST_ARCH),arm64)
	CFLAGS+=-march=armv8-a
endif

# Requires GNU extensions.
CFLAGS+=-std=gnu23

%:
	dh $@ --without autoreconf

override_dh_auto_configure:
	mkdir build_static; cd build_static; ../Configure no-shared $(CONFARGS) $(DEB_HOST_ARCH_OS)-$(DEB_HOST_GNU_CPU); perl configdata.pm -d
	# Debian Perl policy 5.1 (Script Magic)
	mkdir build_shared; cd build_shared; HASHBANGPERL=/usr/bin/perl ../Configure shared $(CONFARGS) $(DEB_HOST_ARCH_OS)-$(DEB_HOST_GNU_CPU); perl configdata.pm -d

override_dh_auto_build-arch:
	$(MAKE) -C build_static all
	ln -sf apps/openssl.pod crypto/crypto.pod ssl/ssl.pod doc/
	$(MAKE) -C build_shared all

override_dh_auto_build-indep:
	$(MAKE) -C build_shared all

override_dh_auto_test:
	$(MAKE) -C build_static test
	$(MAKE) -C build_shared test

execute_before_dh_auto_clean:
	rm -rf build_static build_shared
	rm -f doc/openssl.pod doc/crypto.pod doc/ssl.pod

override_dh_auto_install-indep:
	# Install is only required for the -indep only build, that is if
	# -arch isn't run as well. Otherwise install will fail because
	# usr/lib/ssl/cert is a symlink to a non existing target.
	if [ ! -L debian/tmp/usr/lib/ssl/certs ]; then \
		$(MAKE) -C build_shared install DESTDIR=`pwd`/debian/tmp; \
	fi

override_dh_auto_install-arch:
	$(MAKE) -C build_shared install DESTDIR=`pwd`/debian/tmp

override_dh_installchangelogs:
	dh_installchangelogs CHANGES.md

override_dh_fixperms:
	if [ -d debian/openssl/etc/ssl/private ]; then \
		chmod 700 debian/openssl/etc/ssl/private; \
	fi
	dh_fixperms -a -X etc/ssl/private

override_dh_perl:
	dh_perl -d

override_dh_makeshlibs:
	dh_makeshlibs -a -V -Xengines -Xossl-modules -- -c4

override_dh_shlibdeps:
	dh_shlibdeps -a -L libssl3t64
